/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import eportfoliogenerator.components.HeaderComponent;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author YanLiang Li
 */
public class HeaderView extends HBox{
    HeaderComponent header;
    EPortfolioGeneratorView ui;
    Label headerLabel;
    TextField headerField;
    Button removeBt;
    public HeaderView(HeaderComponent h, EPortfolioGeneratorView iniui){
        header = h;
        ui = iniui;
        
        this.initAll();
        this.initHandlers();
        this.setAlignment(Pos.CENTER);
    }
    
    private void initAll(){
        headerLabel = new Label("Header: ");
        headerField = new TextField();
        headerField.setText(header.getHeader());
        headerField.setPrefWidth(500);
        this.getChildren().addAll(headerLabel, headerField);
        removeBt = this.initChildButton(this, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        
    }
    private void initHandlers(){
        headerField.setOnAction(e -> {
                ui.getPages().getSelectedPage().getHeaderModel().setSelectedHeader(header);
                header.setHeader(headerField.getText());
                refreshIC();
	});
        removeBt.setOnMousePressed(e -> {
                //int indImgModel = ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image);
                ui.getPages().getSelectedPage().getHeaderModel().setSelectedHeader(header);
                int i = (ui.getPages().getSelectedPage().getHeaderModel().getHeaders().indexOf(header));
                int ind = 0;
                int count = -1;
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    //System.out.println(k);
                    if(k.equals("header")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getHeaderModel().removeSelectedHeader();
                ui.getPages().getSelectedPage().removeImageFromlist(count);   
                ui.refresh();
	});
    }
    private void refreshIC(){
        this.getChildren().clear();
        this.initAll();
        this.initHandlers();
    }
     private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
