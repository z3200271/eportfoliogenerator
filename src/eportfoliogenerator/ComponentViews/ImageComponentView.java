/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SELECT;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SELECT;
import eportfoliogenerator.components.ImageComponent;
import eportfoliogenerator.controller.ImageSelectionController;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author YanLiang Li
 */
public class ImageComponentView extends HBox {
    private ImageView imgView;
    private Button browseBt;
    private Button removeBt;
    private TextField heightField;
    private TextField widthField;
    private TextField captionField;
    private Label heightLabel;
    private Label widthLabel;
    private Label floatLabel;
    private Label captionLabel;
    private RadioButton flowLeftRBt;
    private RadioButton flowRightRBt;
    EPortfolioGeneratorView ui;
    ImageComponent image;
    public ImageComponentView(ImageComponent img, EPortfolioGeneratorView iniui){
        ui = iniui;
        image = img;
        this.initImageView();
        this.initAll();
        this.initHandlers();
        this.setAlignment(Pos.CENTER);
    }
    
    public ImageComponent getImageComponent(){
        return image;
    }
    private void initAll(){
        
        GridPane grid = new GridPane();
     
        heightField = new TextField();
        heightField.setPromptText(Integer.toString(image.getHeight()));
        widthField = new TextField();
        widthField.setPromptText(Integer.toString(image.getWidth()));
        heightLabel = new Label("height: ");
        widthLabel = new Label("width: ");
        floatLabel = new Label("Float: ");
        
        grid.addColumn(0, heightLabel);
        grid.addColumn(0, widthLabel);
        grid.addColumn(0, floatLabel);
        grid.addRow(0, heightField);
        grid.addRow(1, widthField);
        
        HBox h = new HBox();
        flowLeftRBt = new RadioButton();
        flowLeftRBt.setText("Float Left");
        flowRightRBt = new RadioButton();
        flowRightRBt.setText("Float Right");
        h.getChildren().addAll(flowLeftRBt, flowRightRBt);
        selectFloat();
        grid.addRow(2, h);
        this.getChildren().add(grid);
    }
    private void selectFloat(){
        if(image.getLeftFlow()){
            flowLeftRBt.setSelected(true);
            flowRightRBt.setSelected(false);
        }
        else{
            flowLeftRBt.setSelected(false);
            flowRightRBt.setSelected(true);
        }
    }
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initImageView(){
        HBox h = new HBox();
        VBox v = new VBox();
        imgView = new ImageView(image.getImage());
        imgView.setFitHeight(160);
        imgView.setFitWidth(260);
        captionLabel = new Label ("Caption: ");
        captionField = new TextField();
        captionField.setPromptText(image.getCaption());
        HBox h1 = new HBox();
        h1.getChildren().addAll(captionLabel, captionField);
        v.getChildren().addAll(imgView, h1);
        
        VBox v1 = new VBox();
        browseBt = this.initChildButton(v1, ICON_SELECT, TOOLTIP_SELECT, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        removeBt = this.initChildButton(v1, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        h.getChildren().addAll(v, v1);
        this.getChildren().add(h);
        
    }
    private void initHandlers(){
        browseBt.setOnMousePressed(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                ImageSelectionController isc = new ImageSelectionController(ui);
                isc.processSelectImageComponent(image);	
                refreshIC();
	});
        
        removeBt.setOnMousePressed(e -> {
                //int indImgModel = ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image);
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                int i = (ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image));
                int ind = 0;
                int count = -1;
                //System.out.println(i);
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    //System.out.println(k);
                    if(k.equals("image")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getImagesModel().removeSelectedImage();
                ui.getPages().getSelectedPage().removeImageFromlist(count);
                
                ui.refresh();
	});
        
        captionField.setOnAction(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                image.setCaption(captionField.getText());
                //ui.refresh();
                refreshIC();
	});
        heightField.setOnAction(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                try{
                    int h = Integer.parseInt(heightField.getText());
                    image.setHeight(h);
                }
                catch(NumberFormatException nfe){
                    ui.getErrorHandler().processError("Wrong input format");
                }
                //ui.refresh();
                refreshIC();
	});
        widthField.setOnAction(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                try{
                    int w = Integer.parseInt(widthField.getText());
                    image.setWidth(w);
                }
                catch(NumberFormatException nfe){
                    ui.getErrorHandler().processError("Wrong input format");
                }
                //ui.refresh();
                refreshIC();
	});
        flowLeftRBt.setOnAction(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                image.setLeftFlow();
                selectFloat();
                //ui.refresh();
                refreshIC();
	});
        
        flowRightRBt.setOnAction(e -> {
                ui.getPages().getSelectedPage().getImagesModel().setSelectedImage(image);
                image.setRightFLow();
                selectFloat();
                //ui.refresh();
                refreshIC();
	});
    }
    
    private void refreshIC(){
        this.getChildren().clear();
        this.initImageView();
        this.initAll();
        this.initHandlers();
    }
}
