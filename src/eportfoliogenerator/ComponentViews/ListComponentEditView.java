/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDITOR_VBOX;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_ADD;
import static eportfoliogenerator.StartupConstants.ICON_SAVE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import eportfoliogenerator.components.ListComponent;
import eportfoliogenerator.components.ListData;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static ssm.StartupConstants.CSS_CLASS_WORKSPACE;

/**
 *
 * @author YanLiang Li
 */
public class ListComponentEditView {
    private ListComponent listComToEdit;
    EPortfolioGeneratorView ui;
    private Stage stage;
    private Scene scene;
    private VBox listEditVBox;
    private ScrollPane listEditSP;
    private Button addItemBt;
    private Button saveBt;
    public ListComponentEditView(ListComponent lc, EPortfolioGeneratorView iui){
       listComToEdit = lc;
       ui = iui;
       listEditVBox = new VBox();
       initListEditVBox();
       initHandlers();
       initWindow();
    }
    private void initListEditVBox(){
        
        listEditVBox.getStyleClass().add("list_edit_view");
        //listEditVBox.getStyleClass().add(CSS_CLASS_WORKSPACE);
        HBox h = new HBox(10);
        addItemBt = this.initChildButton(h, ICON_ADD, "Add Item", CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        saveBt = this.initChildButton(h, ICON_SAVE, "Done", CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        listEditVBox.getChildren().add(h);
        
        for(ListData data: listComToEdit.getListComponent()){
            ListDataView ldv = new ListDataView(data, listComToEdit);
            listEditVBox.getChildren().add(ldv);
        }
    }
    private void initHandlers(){
        addItemBt.setOnMousePressed(e->{
            ListData ld = new ListData();
            listComToEdit.addListData(ld);
            refresh();
        });
        saveBt.setOnAction(e->{
            stage.close();
            ui.refresh();
        });
    }
    private void refresh(){
        listEditVBox.getChildren().clear();
        initListEditVBox();
        this.initHandlers();
    }
    private void initWindow() {
        stage = new Stage();
        // SET THE WINDOW TITLE
        stage.setTitle("Edit List");
       
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

      
        stage.setWidth(300);
        stage.setHeight(500);

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        listEditSP = new ScrollPane();
        listEditSP.setContent(listEditVBox);
        scene = new Scene(listEditSP);
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Indie+Flower");
        scene.getStylesheets().add(STYLE_SHEET_UI);
        stage.setScene(scene);
        stage.show();
    }

     /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     * @param toolbar
     * @param iconFileName
     * @param tooltip
     * @param cssClass
     * @param disabled
     * @return Button
     */
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
