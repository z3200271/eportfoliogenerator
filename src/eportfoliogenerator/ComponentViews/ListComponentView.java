/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_EDIT;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SELECT;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SELECT;
import eportfoliogenerator.components.ListComponent;
import eportfoliogenerator.components.ListData;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author YanLiang Li
 */
public class ListComponentView extends VBox{
    
    EPortfolioGeneratorView ui;
    private ListComponent listComponent;
    private Button editButton;
    private Button removeBt;
    public ListComponentView(ListComponent lc, EPortfolioGeneratorView iniui){
        listComponent = lc;
        ui = iniui;
        
        this.initAll();
        this.initHandlers();
        this.setAlignment(Pos.CENTER);
    }
    
    private void initAll(){
        HBox h = new HBox();
        h.setAlignment(Pos.CENTER);
        h.setPrefWidth(300);
        editButton = this.initChildButton(h, ICON_EDIT, "Edit List  ", CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        removeBt = this.initChildButton(h, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        this.getChildren().add(h);
        int count = 0;
        for(ListData data : listComponent.getListComponent()){
            Label lb = new Label(data.getListData());
            lb.getStyleClass().add("blk_border");
            lb.setMinWidth(300);
            this.getChildren().add(lb);
            count++;
        }
        this.setMaxWidth(300);
        this.getStyleClass().add("blk_border");
        
    }
    private void initHandlers(){
        editButton.setOnMousePressed(e-> {
            ui.getPages().getSelectedPage().getListComponentModel().setSelectedLists(listComponent);
            ListComponentEditView lcdv = new ListComponentEditView(listComponent, ui);
        });
        removeBt.setOnMousePressed(e->{
            ui.getPages().getSelectedPage().getListComponentModel().setSelectedLists(listComponent);
                int i = (ui.getPages().getSelectedPage().getListComponentModel().getLists().indexOf(listComponent));
                int ind = 0;
                int count = -1;
                //System.out.println(i);
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    //System.out.println(k);
                    if(k.equals("list")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getListComponentModel().removeSelectedList();
                ui.getPages().getSelectedPage().removeImageFromlist(count);
                
                ui.refresh();
        });
    }
   
     private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
