/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import eportfoliogenerator.components.ListComponent;
import eportfoliogenerator.components.ListData;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author YanLiang Li
 */
public class ListDataView extends HBox{
    TextField listDataField;
    Button removeBt;
    ListData listData;
    ListComponent listComponent;
    public ListDataView(ListData l, ListComponent lc){
        listData = l;
        listComponent = lc;
        initAll();
        initHandlers();
    }
    
    private void initAll(){
        listDataField = new TextField();
        listDataField.setText(listData.getListData());
        this.getChildren().add(listDataField);
        removeBt = this.initChildButton(this, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);    
    }
    private void initHandlers(){
        listDataField.setOnAction(e->{
            listComponent.setListDataSelected(listData);
            listData.setListData(listDataField.getText());
        });
        removeBt.setOnMousePressed(e->{
            listComponent.setListDataSelected(listData);
            listComponent.removeSelectedListData();
            this.getChildren().clear();
        });
    }
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
