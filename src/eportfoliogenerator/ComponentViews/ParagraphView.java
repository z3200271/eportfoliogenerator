/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_EDIT_HYPERLINK;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SAVE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_HYPERLINK;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SAVE;
import eportfoliogenerator.components.ParagraphComponent;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import eportfoliogenerator.view.HyperLinkDialog;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author YanLiang Li
 */
public class ParagraphView extends HBox{
    ParagraphComponent paragraph;
    EPortfolioGeneratorView ui;
    Label pLabel;
    TextArea pField;
    private Button removeBt;
    private Button saveBt;
    private Button addHyperLinkBt;
    private boolean save;
    public ParagraphView(ParagraphComponent p, EPortfolioGeneratorView iniui){
        paragraph = p;
        ui = iniui;
        save = false;
        this.initAll();
        this.initHandlers();
        this.getStyleClass().add("paragraph_hox");
    }
    
    private void initAll(){
        pLabel = new Label("Paragraph: ");
        pField = new TextArea();
        pField.setText(paragraph.getParagraph()); 
        pField.setMinWidth(500);
        pField.setMinHeight(120);
        VBox v = new VBox();
        saveBt = this.initChildButton(v, ICON_SAVE, TOOLTIP_SAVE, CSS_CLASS_PAGE_EDIT_BUTTONS, true);
        removeBt = this.initChildButton(v, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        addHyperLinkBt = this.initChildButton(v, ICON_EDIT_HYPERLINK, TOOLTIP_ADD_HYPERLINK, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        this.getChildren().addAll(pLabel, pField,v);
        
    }
    private void initHandlers(){
        saveBt.setOnMousePressed(e -> {
                ui.getPages().getSelectedPage().getParagraphModel().setSelectedParagraph(paragraph);
                paragraph.setParagraph(pField.getText());
                System.out.print(paragraph.getParagraph());
                saveBt.setDisable(true);
                refreshIC();
	});
       
        pField.setOnKeyTyped(e ->{
            saveBt.setDisable(false);
        });
        addHyperLinkBt.setOnAction(e->{
            HyperLinkDialog hd = new HyperLinkDialog();
        });
        removeBt.setOnMousePressed(e -> {
                //int indImgModel = ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image);
                ui.getPages().getSelectedPage().getParagraphModel().setSelectedParagraph(paragraph);
                int i = (ui.getPages().getSelectedPage().getParagraphModel().getParagraphs().indexOf(paragraph));
                int ind = 0;
                int count = -1;
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    //System.out.println(k);
                    if(k.equals("paragraph")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getParagraphModel().removeSelectedParagraph();
                ui.getPages().getSelectedPage().removeImageFromlist(count);   
                ui.refresh();
	});
    }
    private void refreshIC(){
        this.getChildren().clear();
        this.initAll();
        this.initHandlers();
    }
     private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
