/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_CREATE_NEW_SLIDESHOW;
import static eportfoliogenerator.StartupConstants.ICON_PLAY;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SELECT;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static eportfoliogenerator.StartupConstants.TOOLTIP_CREATE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SELECT;
import eportfoliogenerator.components.SlideshowComponent;
import eportfoliogenerator.components.VideoComponent;
import eportfoliogenerator.controller.VideoSelectionController;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.io.File;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;
import ssm.error.ErrorHandler;

/**
 *
 * @author YanLiang
 */
public class SlideshowView extends HBox{
    ImageView slideImg;
    Label slideCaption;
    Button makeBt;
    Button removeBt;
    EPortfolioGeneratorView ui;
    SlideshowComponent slideCom;
    public SlideshowView(SlideshowComponent sc, EPortfolioGeneratorView iniui){
        ui = iniui;
        slideCom = sc;
        this.initSlideImage();
        this.initAll();
        this.initHandlers();
        this.setAlignment(Pos.CENTER);
    }
    
    public SlideshowComponent getSlideshowComponent(){
        return slideCom;
    }
    private void initAll(){
        VBox v1 = new VBox();
        makeBt = this.initChildButton(v1, ICON_CREATE_NEW_SLIDESHOW, TOOLTIP_CREATE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        removeBt = this.initChildButton(v1, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        
        this.getChildren().addAll(v1);
    }
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initSlideImage(){
        VBox v = new VBox();
        String path = PATH_SLIDE_SHOW_IMAGES + "DefaultStartSlide.png";
        File file = new File(path);
        try{
           // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    slideImg = new ImageView(new Image(fileURL.toExternalForm()));
        
        } catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("An Unexpected Error has Occured");
	}
        slideCaption = new Label("Caption goes here");
        slideImg.setFitHeight(160);
        slideImg.setFitWidth(260);
        v.getChildren().addAll(slideImg, slideCaption);
        v.setAlignment(Pos.CENTER);
        this.getChildren().add(v);
    }
    private void initHandlers(){
        makeBt.setOnMousePressed(e -> {
               ui.getPages().getSelectedPage().getSlideshoComponentModel().setSelectedSlideshow(slideCom);
               slideCom.makeSlideshow();
               refreshIC();
	});
        
        removeBt.setOnMousePressed(e -> {
                //int indImgModel = ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image);
                ui.getPages().getSelectedPage().getSlideshoComponentModel().setSelectedSlideshow(slideCom);
                int i = (ui.getPages().getSelectedPage().getSlideshoComponentModel().getSlideshows().indexOf(slideCom));
                int ind = 0;
                int count = -1;
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    if(k.equals("slideshow")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getVideoModel().removeSelectedVideo();
                ui.getPages().getSelectedPage().removeSlideshowFromlist(count);
                ui.refresh();
        });
    }
    
    private void refreshIC(){
        this.getChildren().clear();
        this.initSlideImage();
        this.initAll();
        this.initHandlers();
    }
}
