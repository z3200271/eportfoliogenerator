/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.ComponentViews;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_PLAY;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SELECT;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SELECT;
import eportfoliogenerator.components.VideoComponent;
import eportfoliogenerator.controller.VideoSelectionController;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;
import ssm.error.ErrorHandler;

/**
 *
 * @author YanLiang Li
 */
public class VideoView extends HBox{
    private Button browseBt;
    private Button removeBt;
    Media media;
    MediaPlayer mediaPlayer;
    MediaView mediaView;
    String videoURL; 
    Button playBt;
    
    EPortfolioGeneratorView ui;
    VideoComponent video;
    public VideoView(VideoComponent vi, EPortfolioGeneratorView iniui){
        ui = iniui;
        video = vi;
        this.initVideoPlayer();
        this.initAll();
        this.initHandlers();
        this.setAlignment(Pos.CENTER);
    }
    
    public VideoComponent getVideoComponent(){
        return video;
    }
    private void initAll(){
        VBox v1 = new VBox();
        browseBt = this.initChildButton(v1, ICON_SELECT, TOOLTIP_SELECT, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        removeBt = this.initChildButton(v1, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        this.getChildren().addAll(v1);
    }
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initVideoPlayer(){
        VBox v = new VBox();
        v.setAlignment(Pos.CENTER);
        String path = video.getVideoPath() + video.getFileName();
        File file = new File(path);
       
        try {
	    // GET AND SET THE IMAGE
	    videoURL = file.toURI().toString();
	    media = new Media(videoURL);
            mediaPlayer = new MediaPlayer(media);
            mediaView = new MediaView(mediaPlayer);
            mediaView.setFitHeight(260);
            mediaView.setFitWidth(360);
            
            
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("An Unexpected Error has Occured");
	}
        v.getChildren().add(mediaView);
        playBt = this.initChildButton(v, ICON_PLAY, "play from start", null, false);
        this.getChildren().add(v); 
    }
    private void initHandlers(){
        browseBt.setOnMousePressed(e -> {
               ui.getPages().getSelectedPage().getVideoModel().setSelectedVideo(video);
               VideoSelectionController scv = new VideoSelectionController(ui);
               scv.processSelectVideo(video);
               refreshIC();
	});
        
        removeBt.setOnMousePressed(e -> {
                //int indImgModel = ui.getPages().getSelectedPage().getImagesModel().getImages().indexOf(image);
                ui.getPages().getSelectedPage().getVideoModel().setSelectedVideo(video);
                int i = (ui.getPages().getSelectedPage().getVideoModel().getVideos().indexOf(video));
                int ind = 0;
                int count = -1;
                while(i >= ind){
                    count++;
                    String k = ui.getPages().getSelectedPage().getComponentList().get(count);
                    if(k.equals("video")){
                        ind++;
                    }
                }             
                //System.out.println("remove: "+count+ui.getPages().getSelectedPage().getComponentList().get(count));
                ui.getPages().getSelectedPage().getVideoModel().removeSelectedVideo();
                ui.getPages().getSelectedPage().removeVideoFromlist(count);
                
                ui.refresh();
	});
        playBt.setOnMousePressed(e->{
            ui.getPages().getSelectedPage().getVideoModel().setSelectedVideo(video);
            mediaPlayer.seek(Duration.ZERO);
            mediaPlayer.play();
        });
    }
    
    private void refreshIC(){
        this.getChildren().clear();
        this.initVideoPlayer();
        this.initAll();
        this.initHandlers();
    }
}
