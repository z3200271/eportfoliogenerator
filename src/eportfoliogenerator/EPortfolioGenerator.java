/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import eportfoliogenerator.file.EPortfolioFileManager;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author YanLiang Li
 */
public class EPortfolioGenerator extends Application {
    @Override
    public void start(Stage primaryStage) {
       EPortfolioFileManager fileManager = new EPortfolioFileManager();
       EPortfolioGeneratorView ui = new EPortfolioGeneratorView(fileManager);
       ui.startUI(primaryStage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
