/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

/**
 *
 * @author YanLiang Li
 */
public class StartupConstants {
    public static String APP_TITLE = "ePortfolio Generator";
     
    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String UI_PROPERTIES_FILE_NAME_Finnish = "properties_FI.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_SLIDE_SHOWS = PATH_DATA + "slideshows/";
    public static String PATH_EPORTFOLIOS = PATH_DATA + "ePortfolios/";
    public static String PATH_VIDEOS = "./videos/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_SITES = "./ePortfolioTemplates/";
    public static String PATH_SITE_ONE = "./ePortfolioTemplates/ePortfolioSite1.html";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_EPORTFOLIO_IMAGES = PATH_IMAGES + "ePortfolio_images/";
    public static String PATH_CSS = "/eportfoliogenerator/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGenerator.css";
    
    
    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "App_Logo.png";
    public static String ICON_NEW_EPORTFOLIO_GENERATOR = "New.png";
    public static String ICON_LOAD_EPORTFOLIO_GENERATOR = "Load.png";
    public static String ICON_SAVE_EPORTFOLIO_GENERATOR = "Save.png";
    public static String ICON_SAVE_AS_EPORTFOLIO_GENERATOR = "SaveAs.png";
    public static String ICON_EXPORT_EPORTFOLIO_GENERATOR = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD = "Add.png";
    public static String ICON_REMOVE = "Remove.png";
    public static String ICON_SAVE = "Save.png";
    public static String ICON_REFRESH = "Refresh.png";
    public static String ICON_ADD_IMAGE = "Image.png";
    public static String ICON_ADD_VIDEO = "Video.png";
    public static String ICON_ADD_SLIDE_SHOW = "Slideshow.png";
    public static String ICON_ADD_PARAGRAPH = "Paragraph.png";
    public static String ICON_ADD_HEADER = "Header.png";
    public static String ICON_EDIT_HYPERLINK = "Hyperlink.png";
    public static String ICON_ADD_LIST = "List.png";
    public static String ICON_LAYOUT = "Layout.png";
    public static String ICON_EDIT = "Edit.png";
    public static String ICON_CREATE_NEW_SLIDESHOW = "CreateNewSlideshow.png";
    public static String ICON_FONTS = "Font.png";
    public static String ICON_SELECT = "Select.png";
    public static String ICON_PLAY = "Play.png";
    
    // HERE ARE THE BUTTON TOOLTIPS
    public static String TOOLTIP_NEW_EPG = "New";
    public static String TOOLTIP_LOAD_EPG = "Load";
    public static String TOOLTIP_SAVE_EPG = "Save";
    public static String TOOLTIP_SAVE_AS_EPG = "Save as";
    public static String TOOLTIP_EXPORT_EPG = "Export";
    public static String TOOLTIP_EXIT = "Exit";
    public static String TOOLTIP_SAVE = "Save";
    public static String TOOLTIP_ADD_PAGE = "Add Page";
    public static String TOOLTIP_REMOVE_PAGE = "Remove Page";
    public static String TOOLTIP_REFRESH = "Refresh Pages"; 
    public static String TOOLTIP_ADD_IMAGE = "Add Image";  
    public static String TOOLTIP_ADD_VIDEO = "Add Video";  
    public static String TOOLTIP_ADD_SLIDE_SHOW = "Add Slideshow";
    public static String TOOLTIP_ADD_HEADER = "Add Header"; 
    public static String TOOLTIP_ADD_PARAGRAPH = "Add Paragraph";  
    public static String TOOLTIP_ADD_LIST = "Add List";  
    public static String TOOLTIP_ADD_HYPERLINK = "Add Hyperlink";  
    public static String TOOLTIP_REMOVE = "Remove";
    public static String TOOLTIP_SELECT = "Select";
    public static String TOOLTIP_CREATE = "Create New";
    
    
            
            // CSS - FOR THE TOOLBARS
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_PANE = "horizontal_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_PANE = "vertical_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_PAGE_EDITOR_LABEL = "page_edit_label";
    public static String    CSS_CLASS_PAGE_EDITOR_TEXTFIELD = "page_edit_textfield";
    public static String    CSS_CLASS_PAGE_BORDER_BLACK = "blk_border";
    public static String    CSS_CLASS_PAGE_EDIT_BUTTONS = "page_edit_buttons";
    public static String    CSS_CLASS_PAGE_EDIT_TOOLBAR = "page_edit_toolbar";
    public static String    CSS_CLASS_PAGE_GRID = "page_grid";
    public static String    CSS_CLASS_PAGE_EDITOR_VBOX = "page_edit_vbox";
    
}
