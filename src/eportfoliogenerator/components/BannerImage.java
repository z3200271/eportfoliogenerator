/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIO_IMAGES;
import java.io.File;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.error.ErrorHandler;

/**
 *
 * @author YanLiang Li
 */
public class BannerImage {
    String fileName;
    String imagePath;
    public BannerImage(){
        fileName = "DefaultImage.png";
        imagePath = PATH_EPORTFOLIO_IMAGES;
    }
    
    public String getFileName(){
        return fileName;
    }
    public String getImagePath(){
        return imagePath;
    }
    
    public void setFileName(String name){
        fileName = name;
    }
    public void setImagePath(String path){
        imagePath = path;
    }
    
    public Image getBannerImage(){
        String path = imagePath + fileName;
        File file = new File(path);
        Image bannerImage = null;
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    bannerImage = new Image(fileURL.toExternalForm());
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("An Unexpected Error has Occured");
	}
        return bannerImage;
    }
}
