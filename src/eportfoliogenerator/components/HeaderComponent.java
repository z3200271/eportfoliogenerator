/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

/**
 *
 * @author YanLiang Li
 */
public class HeaderComponent {
    private String header;
    
    public HeaderComponent(){
        header = "Enter Header";
    }
    
    public String getHeader(){
        return header;
    }
    public void setHeader(String h){
        header = h;
    }
}
