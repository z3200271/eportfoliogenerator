/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class HeaderModel {
    EPortfolioGeneratorView ui;
    ObservableList<HeaderComponent> headers;
    HeaderComponent selectedHeader;
    
    public HeaderModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        headers = FXCollections.observableArrayList();
        selectedHeader = null;
    }
    
    public boolean isHeaderSelected(){
        return selectedHeader != null;
    }
    
    public void setSelectedHeader(HeaderComponent h){
        selectedHeader = h;
    }
    
    public ObservableList<HeaderComponent> getHeaders(){
        return headers;
    }
    
    public void addHeader(){
        HeaderComponent headerToAdd = new HeaderComponent();
        headers.add(headerToAdd);
    }
    
    public void removeSelectedHeader(){
        if(isHeaderSelected()){
            headers.remove(selectedHeader);
            selectedHeader = null;
        }
    }
}
