/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SELECT;
import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIO_IMAGES;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SELECT;
import eportfoliogenerator.controller.ImageSelectionController;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import ssm.error.ErrorHandler;

/**
 *
 * @author YanLiang Li
 */
public class ImageComponent {
    private String fileName;
    private String imagePath;
    private String caption;
    private int height;
    private int width;
    private boolean leftFlow;
    private HBox imageHBox;
   

    public ImageComponent(){
        fileName = "DefaultImage.png";
        imagePath = PATH_EPORTFOLIO_IMAGES;
        caption = "Caption goes here";
        height = 160;
        width = 260;
        leftFlow = true;     
    }
    public int getHeight(){return height;}
    public int getWidth(){return width;}
    public boolean getLeftFlow(){return leftFlow;}
    public String getFileName(){return fileName;}
    public String getImagePath(){ return imagePath;}
    public HBox getImageHBox(){ return imageHBox; }
    public String getCaption(){return caption;}
    
    public void setFileName(String name){ 
        fileName = name; 
    }
    public void setImagePath(String path){
        imagePath = path;
    }
    public void setHeight(int h){
        height = h;
    }
    public void setWidth(int w){
        width = w;
    }
    public void setCaption(String c){
        caption = c;
    }
    
    public void setLeftFlow(){leftFlow = true;}
    public void setRightFLow(){ leftFlow = false;}
    public boolean isLeftFlow(){
        return leftFlow;
    }
    
    public Image getImage(){
        String path = imagePath + fileName;
        File file = new File(path);
        Image img = null;
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    img = new Image(fileURL.toExternalForm());
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("An Unexpected Error has Occured");
	}
        return img;
    }
}
