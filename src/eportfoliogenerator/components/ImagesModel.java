/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class ImagesModel {
    EPortfolioGeneratorView ui;
    ObservableList<ImageComponent> images;
    ImageComponent selectedImage;
    
    public ImagesModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        images = FXCollections.observableArrayList();
        selectedImage = null;
    }
    
    public boolean isImageSelected(){
        return selectedImage != null;
    }
    
    public void setSelectedImage(ImageComponent img){
        selectedImage = img;
    }
    
    public ObservableList<ImageComponent> getImages(){
        return images;
    }
    
    public void addImage(){
        ImageComponent imageToAdd = new ImageComponent();
        images.add(imageToAdd);
    }
    
    public void removeSelectedImage(){
        if(isImageSelected()){
            images.remove(selectedImage);
            selectedImage = null;
        }
    }
    
    
    
}
