/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class ListComponent {
    ObservableList<ListData> listComponent;
    ListData selectedListData;
    int listDataCounter;
    
    public ListComponent(){
        listComponent = FXCollections.observableArrayList();
        selectedListData = null;
        listDataCounter = 0;
    }
    public ListData getSelectedData(){
        return selectedListData;
    } 
    public int getListDataCounter(){
        return listDataCounter;
    }
    public boolean isListDataSelected(){
        return selectedListData!=null;
    }
    public void setListDataSelected(ListData ld){
        selectedListData = ld;
    }
    
    public ObservableList<ListData> getListComponent(){
        return listComponent;
    }
    
    public void addListData(ListData ldToAdd){
        listComponent.add(ldToAdd);
        listDataCounter++;
    }
    
    public void removeSelectedListData(){
        if(isListDataSelected()){
            listComponent.remove(selectedListData);
            selectedListData = null;
            listDataCounter--;
        }
        else{
            System.out.println("Error removing list data");
        }
    }
}
