/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class ListComponentModel {
    EPortfolioGeneratorView ui;
    ObservableList<ListComponent> lists;
    ListComponent selectedListComponent;
    int listComponentCount;
    
    public ListComponentModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        lists = FXCollections.observableArrayList();
        selectedListComponent = null;
        listComponentCount = 0;
    }
    public ListComponent getSelectedListComponent(){
        return selectedListComponent;
    }
    public boolean isListsSelected(){
        return selectedListComponent != null;
    }
    
    public void setSelectedLists(ListComponent listC){
        selectedListComponent = listC;
    }
    
    public ObservableList<ListComponent> getLists(){
        return lists;
    }
    
    public void addLists(){
        ListComponent listCToAdd = new ListComponent();
        lists.add(listCToAdd);
        listComponentCount++;
    }
    
    public void removeSelectedList(){
        if(isListsSelected()){
            lists.remove(selectedListComponent);
            selectedListComponent = null;
        }
        listComponentCount--;
    }
    
    
}
