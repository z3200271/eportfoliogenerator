/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

/**
 *
 * @author YanLiang Li
 */
public class ParagraphComponent {
    String paragraph;
    
    public ParagraphComponent(){
        paragraph = "Type a paragraph";
    }
    
    public String getParagraph(){
        return paragraph;
    }
    
    public void setParagraph(String s){
        paragraph = s;
    }
}
