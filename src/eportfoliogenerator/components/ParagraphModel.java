/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class ParagraphModel {
    EPortfolioGeneratorView ui;
    ObservableList<ParagraphComponent> paragraphs;
    ParagraphComponent selectedP;
    
    public ParagraphModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        paragraphs = FXCollections.observableArrayList();
        selectedP = null;
    }
    
    public boolean isParagraphSelected(){
        return selectedP != null;
    }
    
    public void setSelectedParagraph(ParagraphComponent p){
        selectedP = p;
    }
    
    public ObservableList<ParagraphComponent> getParagraphs(){
        return paragraphs;
    }
    
    public void addParagraph(){
        ParagraphComponent pToAdd = new ParagraphComponent();
        paragraphs.add(pToAdd);
    }
    
    public void removeSelectedParagraph(){
        if(isParagraphSelected()){
            paragraphs.remove(selectedP);
            selectedP = null;
        }
    }
}
