/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import static eportfoliogenerator.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.file.SlideShowFileManager;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author YanLiang Li
 */
public class SlideshowComponent {
    private SlideShowModel sm;
    private SlideShowMakerView smView;
    private SlideShowFileManager smfm;
    public SlideshowComponent(){
        smfm = new SlideShowFileManager();
        smView = new SlideShowMakerView(smfm);
        sm = new SlideShowModel(smView);
    }
   public void makeSlideshow(){
       smView.startUI();
   }
}
