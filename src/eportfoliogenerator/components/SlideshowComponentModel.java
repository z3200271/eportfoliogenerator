/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author zhenrongzhou
 */
public class SlideshowComponentModel {
    EPortfolioGeneratorView ui;
    ObservableList<SlideshowComponent> slideshows;
    SlideshowComponent selectedSlideshow;
    
    public SlideshowComponentModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        slideshows = FXCollections.observableArrayList();
        selectedSlideshow = null;
    }
    
    public boolean isSlideshowSelected(){
        return selectedSlideshow != null;
    }
    
    public void setSelectedSlideshow(SlideshowComponent sc){
        selectedSlideshow = sc;
    }
    
    public ObservableList<SlideshowComponent> getSlideshows(){
        return slideshows;
    }
    
    public void addSlideshow(){
        SlideshowComponent videoToAdd = new SlideshowComponent();
        slideshows.add(videoToAdd);
    }
    
    public SlideshowComponent getSelctedSlideshow(){
        return selectedSlideshow;
    }
    
    public void removeSelectedSlideshow(){
        if(isSlideshowSelected()){
            slideshows.remove(selectedSlideshow);
            selectedSlideshow = null;
        }
    }
    
}
