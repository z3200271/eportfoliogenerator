/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import static eportfoliogenerator.StartupConstants.PATH_VIDEOS;

/**
 *
 * @author YanLiang Li
 */
public class VideoComponent {
    private String videoPath;
    private String fileName;
    
    public VideoComponent(){
        videoPath = PATH_VIDEOS;
        fileName = "sample_video.mp4";
    }
    
    public String getVideoPath(){
        return videoPath;
    }
    public String getFileName(){
        return fileName;
    }
    public void setVideoPath(String s){
        videoPath = s;
    }
    public void setFileName(String s){
        fileName = s;
    }
}
