/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.components;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author YanLiang Li
 */
public class VideoModel {
    EPortfolioGeneratorView ui;
    ObservableList<VideoComponent> videos;
    VideoComponent selectedVideo;
    
    public VideoModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        videos = FXCollections.observableArrayList();
        selectedVideo = null;
    }
    
    public boolean isVideoSelected(){
        return selectedVideo != null;
    }
    
    public void setSelectedVideo(VideoComponent img){
        selectedVideo = img;
    }
    
    public ObservableList<VideoComponent> getVideos(){
        return videos;
    }
    
    public void addVideo(){
        VideoComponent videoToAdd = new VideoComponent();
        videos.add(videoToAdd);
    }
    
    public VideoComponent getSelctedVideo(){
        return selectedVideo;
    }
    
    public void removeSelectedVideo(){
        if(isVideoSelected()){
            videos.remove(selectedVideo);
            selectedVideo = null;
        }
    }
    
}
