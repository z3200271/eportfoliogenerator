/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIOS;
import eportfoliogenerator.error.EPGErrorHandler;
import eportfoliogenerator.file.EPortfolioFileManager;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.model.PageModel;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.io.IOException;
import eportfoliogenerator.view.YesNoCancelDialog;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import static ssm.file.SlideShowFileManager.SLASH;
/**
 *
 * @author YanLiang Li
 */
public class FileController {
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private EPortfolioGeneratorView ui;
    private EPortfolioFileManager eportfolioIO;
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    //private SlideShowFileManager slideShowIO;
    
    // THIS ONE EXPORTS OUR SITE
    //private EPorfolioSiteExporter siteExporter;
    
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initUI  
     */
    public FileController(EPortfolioGeneratorView initUI, EPortfolioFileManager initFM) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        eportfolioIO = initFM;
        //slideShowIO = initSlideShowIO;
	//siteExporter = initSiteExporter;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateFileToolbarControls(saved);
    }
    
        /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewEPorfolioGeneratorRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                PageModel pages = ui.getPages();
		pages.reset();
                
                this.markAsEdited();
                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                
                ui.refresh();
            }
        } catch (Exception ioe) {
            //ErrorHandler eH = ui.getErrorHandler();
        }
    }
    
    public void handleLoadEPortfolioGeneratorRequest(){
        boolean continueToOpen = true;
        if (!saved) {
            try {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            } catch (IOException ex) {
                EPGErrorHandler ii = ui.getErrorHandler();
                ii.processError("Error wrting Json file");
            }
        }
        if (continueToOpen) {
            FileChooser eportfolioFileChooser = new FileChooser();
            eportfolioFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIOS));
            FileChooser.ExtensionFilter jsonFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.JSON");
            eportfolioFileChooser.getExtensionFilters().add(jsonFilter);
            File selectedFile = eportfolioFileChooser.showOpenDialog(ui.getWindow());
        // eportfolioFileChooser.showSaveDialog(ui.getWindow());
        }
        
    }
    public void handleSaveEPortfolioGeneratorRequest(){
        try{ 
            PageModel eportToSave = ui.getPages();
            String epgPath = PATH_EPORTFOLIOS + SLASH + eportToSave.getEportfolioName();
            File epgFile = new File(epgPath);
            if(epgFile.exists())
                epgFile.delete();
            
            epgFile.mkdirs();
            
            for(Page page : eportToSave.getPages()){
                String path = epgPath + SLASH + page.getPageTitle();
                File file = new File(path);           
                file.mkdirs();
                String pgPath = path + "/" + page.getPageTitle();
                eportfolioIO.saveEPortfolio(page, pgPath);
            }
            saved = true;
            ui.updateFileToolbarControls(saved);
        } catch (IOException ex) {
            EPGErrorHandler ii = ui.getErrorHandler();
            ii.processError("Error wrting Json file");
        }
    }
    public void handleSaveAsEPortfolioGeneratorRequest(){
        FileChooser eportfolioFileChooser = new FileChooser();
        eportfolioFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIOS));
        //File selectedFile = eportfolioFileChooser.showOpenDialog(ui.getWindow());
        eportfolioFileChooser.showSaveDialog(ui.getWindow());
    }
    public void handleExportEPortfolioGeneratorRequest(){
        
    }
        /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (Exception ioe) {
            
        }
    }
    
    
    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
	YesNoCancelDialog yesNoCancelDialog = new YesNoCancelDialog(ui.getWindow());
        yesNoCancelDialog.show(("Save unsaved work?"));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();	
	boolean saveWork = selection.equals(YesNoCancelDialog.YES);

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            this.handleSaveEPortfolioGeneratorRequest();// save work
            ui.refresh();
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
            
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
}
