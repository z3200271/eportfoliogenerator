/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;


import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIO_IMAGES;
import eportfoliogenerator.components.ImageComponent;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import eportfoliogenerator.view.WorkspaceView;
import java.io.File;
import javafx.stage.FileChooser;


/**
 * This controller provides a controller for when the user chooses to
 * select an image for the slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class ImageSelectionController {
    EPortfolioGeneratorView ui;
    /**
     * Default contstructor doesn't need to initialize anything
     */
    public ImageSelectionController(EPortfolioGeneratorView initUi) {   
	ui = initUi;
    }
    
    public void processSelectBannerImage(Page pageToEdit, WorkspaceView view){
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIO_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    pageToEdit.getBanner().setImagePath(path);
            pageToEdit.getBanner().setFileName(fileName);
	    view.updateBannerImage();
	    //ui.updateFileToolbarControls(false);
	}
    }
    
    public void processSelectImageComponent(ImageComponent imageToEdit){
         FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_EPORTFOLIO_IMAGES));
        // LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    imageToEdit.setImagePath(path);
            imageToEdit.setFileName(fileName);
            //ui.refresh();
	    //ui.updateFileToolbarControls(false);
	}
    }
}
