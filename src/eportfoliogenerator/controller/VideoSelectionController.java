/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import eportfoliogenerator.ComponentViews.VideoView;
import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIO_IMAGES;
import static eportfoliogenerator.StartupConstants.PATH_VIDEOS;
import eportfoliogenerator.components.VideoComponent;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import eportfoliogenerator.view.WorkspaceView;
import java.io.File;
import javafx.stage.FileChooser;

/**
 * This controller provides a controller for when the user chooses to
 * select an image for the slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class VideoSelectionController {
    EPortfolioGeneratorView ui;
    
    /**
     * Default contstructor doesn't need to initialize anything
     */
    public VideoSelectionController(EPortfolioGeneratorView initUi) {   
	ui = initUi;
    }
    
    /**
     * This function provides the response to the user's request to
     * select an image.
     * 
     * @param slideToEdit - Slide for which the user is selecting an image.
     * 
     * @param view The user interface control group where the image
     * will appear after selection.
     */
    public void processSelectVideo(VideoComponent videoToEdit) {
	FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_VIDEOS));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	imageFileChooser.getExtensionFilters().addAll(mp4Filter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    videoToEdit.setVideoPath(path);
            videoToEdit.setFileName(fileName);
	}
    }
}
