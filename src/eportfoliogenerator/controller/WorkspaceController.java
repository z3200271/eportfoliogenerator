/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.controller;

import eportfoliogenerator.model.Page;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import eportfoliogenerator.view.WorkspaceView;

/**
 *
 * @author YanLiang Li
 */
public class WorkspaceController {
     // THE APP UI
    private EPortfolioGeneratorView ui;
    // THE Workpace UI
    private WorkspaceView workspace;
    Page page;

    
    public WorkspaceController(EPortfolioGeneratorView initUi, WorkspaceView initWorkspace){
        ui = initUi;
        workspace = initWorkspace;
        page = ui.getPages().getSelectedPage();
    }
   
    public void processAddImageRequest(){
        page.getImagesModel().addImage();
        page.addImageToList();
        workspace.loadAll(page);
    }
    
    public void proceeAddVideoRequest(){
        page.getVideoModel().addVideo();
        page.addVideoToList();
        workspace.loadAll(page);
    }
    
    public void proceeAddSlideshowRequest(){
        page.getSlideshoComponentModel().addSlideshow();
        page.addSlideshowToList();
        workspace.loadAll(page);
    }
    public void proceeAddHeaderRequest(){
       page.getHeaderModel().addHeader();
       page.addHeaderToList();
       workspace.loadAll(page);
    }
    public void proceeAddParagraphRequest(){
      page.getParagraphModel().addParagraph();
      page.addParagraphToList();
      workspace.loadAll(page);
    }
    public void proceeEditHyperlinkRequest(){
   
    }
    public void proceeAddListRequest(){
        page.getListComponentModel().addLists();
        page.addListsToList();
        workspace.loadAll(page);
    }
}
