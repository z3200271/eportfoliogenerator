/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.error;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.scene.control.Alert;

/**
 *
 * @author YanLiang Li
 */
public class EPGErrorHandler {
     // APP UI
    private EPortfolioGeneratorView ui;
    
    // KEEP THE APP UI FOR LATER
    public EPGErrorHandler(EPortfolioGeneratorView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(String errorType)
    {
        // GET THE FEEDBACK TEXT
        String errorFeedbackText = errorType;
             
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        Alert alertDialog = new Alert(Alert.AlertType.WARNING, errorFeedbackText);
	alertDialog.showAndWait();
    }    
}
