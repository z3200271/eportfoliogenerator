/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.file;

import eportfoliogenerator.model.Page;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/**
 *
 * @author YanLiang Li
 */
public class EPortfolioFileManager {
     // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_PAGES = "pages";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    EPortfolioGeneratorView ui;
    
    /**
     * This method saves all the data associated with a slide show to
     * a JSON file.
     * 
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    public void saveEPortfolio(Page pageToSave, String path) throws IOException {
        OutputStream os;
        // BUILD THE FILE PATH
            String p = path + JSON_EXT;
            // INIT THE WRITER
            
            os = new FileOutputStream(p);
           
            JsonWriter jsonWriter = Json.createWriter(os);  
            
            // BUILD THE SLIDES ARRAY
           // JsonArray eporfolioJsonArray = makeSlidesJsonArray(eportfolioToSave.getPages());
        
            // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
            JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, pageToSave.getPageTitle())
                            .add("studenName", pageToSave.getStudentName())
                    .add("banerImage", (pageToSave.getBanner().getImagePath() + pageToSave.getBanner().getFileName()))
            .build();
            
            jsonWriter.writeObject(courseJsonObject);    
         
    }

    private JsonArray makeSlidesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
	    JsonObject jso = makeEPortfolioJsonObject(page);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    
    private JsonObject makeEPortfolioJsonObject(Page page) {
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_PAGES, page.getPageTitle())
                .build();
	return jso;
    }
    
    
    public static void copyDirectory(File sourceLocation , File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }
             String[] children = sourceLocation.list();
             for (int i=0; i<children.length; i++) {
                 copyDirectory(new File(sourceLocation, children[i]),
                         new File(targetLocation, children[i]));
             }
         } else {   
             InputStream in = new FileInputStream(sourceLocation);
             OutputStream out = new FileOutputStream(targetLocation);
             // Copy the bits from instream to outstream
             byte[] buf = new byte[1024];
             int len;
             while ((len = in.read(buf)) > 0) {
                 out.write(buf, 0, len);
             }
             in.close();
             out.close();
         }
    }
    
    public static void deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                        System.out.println("Delete" );
                    }
                }
            }
        }
    }
}
