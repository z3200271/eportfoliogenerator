/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import eportfoliogenerator.components.ImagesModel;
import eportfoliogenerator.components.BannerImage;
import eportfoliogenerator.components.HeaderModel;
import eportfoliogenerator.components.ListComponentModel;
import eportfoliogenerator.components.ParagraphModel;
import eportfoliogenerator.components.SlideshowComponentModel;
import eportfoliogenerator.components.VideoModel;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * All data contain in a single page
 * @author ylli
 */
public class Page {

    private String pageTitle;
    private String studentName;
    private String footer;
    private BannerImage banImg;
    private int layout;
    private int color;
    private int font;
    
    private ImagesModel imgModel;
    private int imageCount;
    private HeaderModel headerModel;
    private int headerCount;
    private ParagraphModel paragraphModel;
    private int paragraphCount;
    private VideoModel videoModel;
    private int videoCount;
    private SlideshowComponentModel slidesModel;
    private int slideshowCount;
    private ListComponentModel listModel;
    private int listsCount;
    
    
    
    
    private EPortfolioGeneratorView ui;
    private ObservableList<String> componentList;
    public Page(EPortfolioGeneratorView initui){
        ui = initui;
        pageTitle = "default title";
        studentName = "defualt name";
        footer = "footer goes here";
        banImg = new BannerImage();
        imgModel = new ImagesModel(ui);
        headerModel = new HeaderModel(ui);
        paragraphModel= new ParagraphModel(ui);
        videoModel = new VideoModel(ui);
        slidesModel = new SlideshowComponentModel(ui);
        listModel = new ListComponentModel(ui);
        
        componentList = FXCollections.observableArrayList();
        imageCount = 0;
        headerCount = 0;
        paragraphCount = 0;
        videoCount = 0;
        slideshowCount = 0;
        listsCount = 0;
        layout = -1;
        color = -1;
        font = -1;
    }
    
    public int getColor(){
        return color;
    }
    public void setColor(int i){
        color = i;
    }
    public int getLayout(){
        return layout;
    }
     public void setLayout(int i){
        layout = i;
    }
    public int getFont(){
        return font;
    }
     public void setFont(int i){
        font = i;
    }
    public void addImageToList(){
        String str = "image";
        componentList.add(str);
        imageCount++;
    }
    public void removeImageFromlist(int inde){
       componentList.remove(inde);
       imageCount--;
    }
    public void addHeaderToList(){
        String str = "header";
        componentList.add(str);
        headerCount++;
    }
    public void removeHeaderFromlist(int inde){
       componentList.remove(inde);
       headerCount--;
    }
    public void addParagraphToList(){
        String str = "paragraph";
        componentList.add(str);
        paragraphCount++;
    }
    public void removeParagraphFromlist(int inde){
       componentList.remove(inde);
       paragraphCount--;
    }
    public void addVideoToList(){
        String str = "video";
        componentList.add(str);
        videoCount++;
    }
    public void removeVideoFromlist(int inde){
       componentList.remove(inde);
       videoCount--;
    }
    
    public void addSlideshowToList(){
        String str = "slideshow";
        componentList.add(str);
        slideshowCount++;
    }
    public void removeSlideshowFromlist(int inde){
       componentList.remove(inde);
       slideshowCount--;
    }
    public void addListsToList(){
        String str = "list";
        componentList.add(str);
        listsCount++;
    }
    public void removeListsFromlist(int inde){
       componentList.remove(inde);
       listsCount--;
    }
    
    public ObservableList<String> getComponentList(){
        return componentList;
    }
    // ACCESSOR METHODS
    public String getPageTitle(){return pageTitle;}
    public String getStudentName(){return studentName;}
    public String getFooter(){ return footer;}
    public BannerImage getBanner(){return banImg;}
    public int getImageCount(){return imageCount;}
    public int getHeaderCount(){return headerCount;}
    public int getParagraphCount(){return paragraphCount;}
    public int getVideoCount(){return videoCount;}
    public int getSideshowCount(){return slideshowCount;}
    public int getListCount(){return listsCount;}
    
    public ImagesModel getImagesModel(){
        return imgModel;
    }
    public HeaderModel getHeaderModel(){
        return headerModel;
    }
    public ParagraphModel getParagraphModel(){
        return paragraphModel;
    }
    public VideoModel getVideoModel(){
        return videoModel;
    }
    public SlideshowComponentModel getSlideshoComponentModel(){
        return slidesModel;
    }
    public ListComponentModel getListComponentModel(){
        return listModel;
    }

    // MUTATOR METHODS
    public void setPageTitle(String title){ pageTitle = title;}
    public void setStudentName(String name){ studentName = name;}
    public void setFooter(String foot){ footer = foot;}
}
