/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import eportfoliogenerator.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class manages all the data associated with a page
 * @author ylli
 */
public class PageModel {
    private String ePortfolioName;
    EPortfolioGeneratorView ui;
    ObservableList<Page> pages;
    Page selectedPage;
   
    
    public PageModel(EPortfolioGeneratorView initUi){
        ui = initUi;
        pages = FXCollections.observableArrayList();
        this.reset();
        ePortfolioName = "untitled";
    }
    public String getEportfolioName(){
        return ePortfolioName;
    }
    public void setEPortfolioName(String name){
        ePortfolioName = name;
    }
    
    public boolean isPageSelected(){
        return selectedPage != null;
    }
    
    public ObservableList<Page> getPages(){
        return pages;
    }
    
    public Page getSelectedPage(){
        return selectedPage;
    }
    
    
    public void setSelectedPage(Page page){
        selectedPage = page;
    }
    
    public void reset(){
        pages.clear();
        selectedPage = null;
    }
    
    public void addPage(){
        Page pageToAdd = new Page(ui);
        pages.add(pageToAdd);
    }
    
    public void removeSelectedPage(){
        if(isPageSelected()){
            pages.remove(selectedPage);
        }
    }
}
