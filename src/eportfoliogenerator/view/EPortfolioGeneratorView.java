/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.APP_TITLE;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_PANE;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_PANE;
import static eportfoliogenerator.StartupConstants.ICON_ADD;
import static eportfoliogenerator.StartupConstants.ICON_EXIT;
import static eportfoliogenerator.StartupConstants.ICON_EXPORT_EPORTFOLIO_GENERATOR;
import static eportfoliogenerator.StartupConstants.ICON_LOAD_EPORTFOLIO_GENERATOR;
import static eportfoliogenerator.StartupConstants.ICON_NEW_EPORTFOLIO_GENERATOR;
import static eportfoliogenerator.StartupConstants.ICON_REFRESH;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.ICON_SAVE_AS_EPORTFOLIO_GENERATOR;
import static eportfoliogenerator.StartupConstants.ICON_SAVE_EPORTFOLIO_GENERATOR;
import static eportfoliogenerator.StartupConstants.PATH_EPORTFOLIOS;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_PAGE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_EXIT;
import static eportfoliogenerator.StartupConstants.TOOLTIP_EXPORT_EPG;
import static eportfoliogenerator.StartupConstants.TOOLTIP_LOAD_EPG;
import static eportfoliogenerator.StartupConstants.TOOLTIP_NEW_EPG;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REFRESH;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE_PAGE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SAVE_AS_EPG;
import static eportfoliogenerator.StartupConstants.TOOLTIP_SAVE_EPG;
import eportfoliogenerator.controller.FileController;
import eportfoliogenerator.error.EPGErrorHandler;
import eportfoliogenerator.file.EPortfolioFileManager;
import static eportfoliogenerator.file.EPortfolioFileManager.deleteDirectory;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.model.PageModel;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 *
 * @author YanLiang Li
 */
public class EPortfolioGeneratorView {
     // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane epgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button saveEPortfolioButton;
    Button saveAsEPortfolioButton;
    Button exportEPortfolioButton;
    Button exitButton;
    
    // WORKSPACE
    BorderPane workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane siteToolbarPane;
    Button addPageButton;
    Button removePageButton;
    Button selectPageButton;
    
    //GO TOP OF THE WORKSPACE
    TabPane workspaceModeToolbar;
    Tab selectPageEditorWorkspace;
    Tab selectSiteViewerWorkspace;
    
    TabPane pagesTab;
    FlowPane refreshPane;
    Button refreshPagesBt;
    
   // WorkspaceView wspcView;
    
    private int mode = 0;
    private int currentSelectedIndex;
     // THIS IS THE PAGES WE'RE WORKING WITH
    PageModel pages;
    
    // THIS IS FOR SAVING AND LOADING
    EPortfolioFileManager fileManager;
    // THIS IS FOR EXPORTING 
    //EPorfolioSiteExporter siteExporter;
    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private EPGErrorHandler errorHandler;
    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    public EPortfolioGeneratorView(EPortfolioFileManager initFileManage) {
        pages = new PageModel(this);
        currentSelectedIndex = 0;
        errorHandler = new EPGErrorHandler(this);
        fileManager = initFileManage;
    }
    public EPGErrorHandler getErrorHandler(){
        return errorHandler;
    }
    public PageModel getPages(){
        return pages;
    }
    
    public Stage getWindow() {
	return primaryStage;
    }
  
    
    /**
    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }
    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     *
     */
    public void startUI(Stage initPrimaryStage) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(APP_TITLE);
    }
    
     /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
	fileToolbarPane.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	newEPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO_GENERATOR, TOOLTIP_NEW_EPG,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadEPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO_GENERATOR,	TOOLTIP_LOAD_EPG,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO_GENERATOR, TOOLTIP_SAVE_EPG,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	saveAsEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_EPORTFOLIO_GENERATOR, TOOLTIP_SAVE_AS_EPG,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exportEPortfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT_EPORTFOLIO_GENERATOR, TOOLTIP_EXPORT_EPG,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    
    private void initWorkspace(){
        workspace = new BorderPane();
        pagesTab = new TabPane();
        pagesTab.getStyleClass().add("all_tabs");
        siteToolbarPane = new FlowPane();
        
        addPageButton = this.initChildButton(siteToolbarPane, ICON_ADD, TOOLTIP_ADD_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        removePageButton = this.initChildButton(siteToolbarPane, ICON_REMOVE, TOOLTIP_REMOVE_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        workspace.setLeft(siteToolbarPane);
        
        refreshPane = new FlowPane();
        refreshPagesBt = this.initChildButton(refreshPane, ICON_REFRESH, TOOLTIP_REFRESH, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        workspace.setRight(refreshPane);
        refreshPane.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
        workspace.setCenter(pagesTab);
        siteToolbarPane.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
    }
    
    public void pageEditMode(){
        mode = 0;
    }
    public void siteViewMode(){
        mode = 1;
    }
    
    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this,fileManager);
	newEPortfolioButton.setOnAction(e -> {
	    fileController.handleNewEPorfolioGeneratorRequest();
	});
	
        loadEPortfolioButton.setOnAction(e -> {
            fileController.handleLoadEPortfolioGeneratorRequest();
        });
        saveEPortfolioButton.setOnAction(e -> {
            fileController.handleSaveEPortfolioGeneratorRequest();
        });
        saveAsEPortfolioButton.setOnAction(e -> {
            fileController.handleSaveAsEPortfolioGeneratorRequest();
        });
        exportEPortfolioButton.setOnAction(e -> {
            fileController.handleExportEPortfolioGeneratorRequest();
        });
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
        
	addPageButton.setOnAction(e -> {
	    processAddPageRequest();
            this.updateExportBt();
	});
        removePageButton.setOnAction(e -> {
	    processRemovePageRequest();
            this.updateExportBt();
	});
	
        refreshPagesBt.setOnAction(e -> {
	    this.reloadWorkspacePane();
	});
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        
        primaryStage.setTitle(windowTitle);
       
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        epgPane = new BorderPane();
        //epgPane.getStyleClass().add(CSS_CLASS_WORKSPACE);
        epgPane.setTop(fileToolbarPane);	
        primaryScene = new Scene(epgPane);
        primaryScene.getStylesheets().add("https://fonts.googleapis.com/css?family=Indie+Flower");
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

     /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     * @param toolbar
     * @param iconFileName
     * @param tooltip
     * @param cssClass
     * @param disabled
     * @return Button
     */
    public Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
     /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateFileToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	epgPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveEPortfolioButton.setDisable(saved);
        saveAsEPortfolioButton.setDisable(saved);
        this.updateExportBt();
    }
    private void updateExportBt(){
        if (pages.getPages().size() > 0){
            exportEPortfolioButton.setDisable(false);
        }
        else{
            exportEPortfolioButton.setDisable(true);
        }
    }
    
    private void processAddPageRequest(){
        PageModel p1 = this.getPages();
	p1.addPage();
        this.reloadWorkspacePane();
        this.updataRemovePage();
    }
    
    private void processRemovePageRequest(){
        PageModel p1 = this.getPages();
	p1.removeSelectedPage();
	this.reloadWorkspacePane();
        this.updataRemovePage();
    }
    
    public void reloadWorkspacePane(){
        pagesTab.getTabs().clear();
        for (Page page : pages.getPages()) {
            Tab tab = new Tab();
            tab.setText(page.getPageTitle());
            tab.getStyleClass().add("all_tabs");
            pagesTab.getTabs().add(tab);
            pagesTab.getSelectionModel().select(currentSelectedIndex);
            pages.setSelectedPage(pages.getPages().get(pagesTab.getSelectionModel().getSelectedIndex()));
            WorkspaceView wspcView = new WorkspaceView(this);
            wspcView.loadAll(page);
            tab.setContent(wspcView);   
        }
        pagesTab.setOnMouseClicked(e -> {
                currentSelectedIndex  = pagesTab.getSelectionModel().getSelectedIndex();
                this.reloadWorkspacePane();
                pages.setSelectedPage(pages.getPages().get(currentSelectedIndex));            
                pagesTab.getSelectionModel().select(currentSelectedIndex);  
		        
	});
    }
    public void refresh(){
        pagesTab.getTabs().clear();
        for (Page page : pages.getPages()) {
            Tab tab = new Tab();
            tab.setText(page.getPageTitle());
            pagesTab.getTabs().add(tab);
            pagesTab.getSelectionModel().select(currentSelectedIndex);
            tab.getStyleClass().add("all_tabs");
            pages.setSelectedPage(pages.getPages().get(pagesTab.getSelectionModel().getSelectedIndex()));
           // wspcView = this.getWorkspaceView();
            WorkspaceView wspcView = new WorkspaceView(this);
            wspcView.loadAll(page);
            tab.setContent(wspcView);   
        }
        pages.setSelectedPage(pages.getPages().get(currentSelectedIndex));            
        pagesTab.getSelectionModel().select(currentSelectedIndex);  
        
    }
    public void updataRemovePage(){
        if (pages.getPages().size() > 0){
            removePageButton.setDisable(false);
        }
        else
            removePageButton.setDisable(true);
    }
}
    
    
