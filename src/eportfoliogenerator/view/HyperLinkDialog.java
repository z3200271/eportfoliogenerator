/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_SAVE;
import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author YanLiang
 */
public class HyperLinkDialog extends Stage{
    private Scene scene;
    private Label hyperLinkLabel;
    private TextField hyperlinkField;
    private Button saveBt;
    
    public HyperLinkDialog(){
        HBox h = new HBox(8);
        hyperLinkLabel = new Label("link");
        hyperlinkField = new TextField();
        hyperlinkField.setPromptText("enter link");
        hyperlinkField.setOnAction(e->{this.close();});
        h.getChildren().addAll(hyperLinkLabel, hyperlinkField);
        saveBt = this.initChildButton(h, ICON_SAVE, "SAVE", CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        saveBt.setOnAction(e->{this.close();});
        
        hyperLinkLabel.getStyleClass().add("page_edit_label");
        hyperlinkField.getStyleClass().add("page_edit_textfield");
        hyperlinkField.setMinWidth(360);
        h.setAlignment(Pos.CENTER);
        //h.getStyleClass().add("page_edit_vbox");
        
       
        scene = new Scene(h, 600, 100);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(scene);
        this.showAndWait();
        
       
    }
    
    private Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imgPath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imgPath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        ImageView v = new ImageView(buttonImage);
        v.setFitHeight(36);
        v.setFitWidth(36);
        button.setGraphic(v);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
}
