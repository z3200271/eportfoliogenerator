/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author YanLiang Li
 */
public class PromptToSaveView {
    Stage newStage;
    Scene newScene;
    BorderPane pane;
    Button saveBt;
    Button exitBt;
    Boolean chooseSave;
    
    public PromptToSaveView(){
        newStage = new Stage();
        chooseSave = false;
    }
    
    public boolean chooseToSave(){
        return chooseSave;
    }
    
    /**
     * Initialize the asking language window
     * 
     * @param windowTitle
     */
    public void startUI(){
        initPane();
        initHandler();
        initWindow();
    }
    
    private void initPane(){
        pane = new BorderPane();
        
        FlowPane northPane = new FlowPane();
        Label newLabel = new Label("Save Current Work");
        northPane.setAlignment(Pos.CENTER);
        northPane.getChildren().add(newLabel);
        pane.setTop(northPane);
        
        HBox southPane = new HBox(25);
        southPane.setAlignment(Pos.CENTER);
        saveBt = new Button("Yes");
        exitBt = new Button("No");
        southPane.getChildren().addAll(saveBt, exitBt);
        pane.setBottom(southPane);
    }
    
    private void initWindow() {
        newStage.setTitle("ALERT");
        newScene = new Scene(pane, 500, 300);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }

    
    public void initHandler(){
        saveBt.setOnAction(e -> tryToSave());
        exitBt.setOnAction(e -> notToSave());
    }
    
    private void tryToSave(){
        chooseSave = true;
        newStage.close();
    }
    
    private void notToSave(){
        chooseSave = false;
        newStage.close();
    }
}

