/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Page;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author YanLiang Li
 */
public class StyleSelection extends VBox{
    ComboBox colorSelection;
    ComboBox layoutSelection;
    ComboBox fontSelection;
  
    EPortfolioGeneratorView ui;
    Page page;
    public StyleSelection(EPortfolioGeneratorView ini){     
        ui = ini;
        page = ui.getPages().getSelectedPage();
        initAll();
        initHandlers();
    }
    
     
    private void initAll(){
        ObservableList<String> colorOptions = 
        FXCollections.observableArrayList(
        "Color 1 - red",
        "Color 2 - green",
        "Color 3 - blue",
        "Color 4 - orange",
        "Color 5 - yellow"
        );
        colorSelection = new ComboBox(colorOptions);
        if(page.getColor() > -1){
            colorSelection.getSelectionModel().select(page.getColor());
        }
        else{
            colorSelection.promptTextProperty().set("Choose Color");
        }
        
        ObservableList<String> layoutOptions = 
        FXCollections.observableArrayList(
        "Layout 1 - top",
        "Layout 2 - below",
        "Layout 3 - left",
        "Layout 4 - below-left",
        "Layout 5 - middle"
        );
        layoutSelection = new ComboBox(layoutOptions);
        if(page.getLayout() > -1){
            layoutSelection.getSelectionModel().select(page.getLayout());
        }
        else{
        layoutSelection.promptTextProperty().set("Choose Layout(Nav position)");
        }
        ObservableList<String> fontOptions = 
        FXCollections.observableArrayList(
        "Font 1 - Indie Flower",
        "Font 2 - Rokkitt",
        "Font 3 - Passion One",
        "Font 4 - Black Ops One",
        "Font 5 - Comfortaa"
        );
        fontSelection = new ComboBox(fontOptions);
        if(page.getFont() > -1){
            fontSelection.getSelectionModel().select(page.getFont());
        }
        else{
        fontSelection.promptTextProperty().set("Choose Font");
         }
        this.setSpacing(10);
        this.getChildren().addAll(colorSelection, layoutSelection, fontSelection);
        
    }
    
    private void initHandlers(){
        String sourcefolder = "./sample/";
        String desfolder = "./ePortfolioTemplates/css/";
        colorSelection.setOnAction(e->{
                int i = (colorSelection.getSelectionModel().getSelectedIndex());
                page.setColor(i);
            try {
                String source = sourcefolder + "color"+(i+1)+".css";
                
                File sourcefile = new File(source);
                Path from = sourcefile.toPath();
                //System.out.println(from);
                String des = desfolder + "color.css";
               
                File desfile = new File(des);
                Path to = desfile.toPath();
                //System.out.println(to);
                
                Files.copy(from, to, REPLACE_EXISTING);
            } catch (IOException ex) {
                ui.getErrorHandler().processError("Error copying color style");
            }
        });
        layoutSelection.setOnAction(e->{
            int i = (layoutSelection.getSelectionModel().getSelectedIndex());
            page.setLayout(i);
            try {
                String source = sourcefolder + "layout"+(i+1)+".css";
                File sourcefile = new File(source);
                Path from = sourcefile.toPath();
                String des = desfolder + "layout.css";
                File desfile = new File(des);
                Path to = desfile.toPath();         
                Files.copy(from, to, REPLACE_EXISTING);
            } catch (IOException ex) {
                ui.getErrorHandler().processError("Error copying layout style");
            }
        });
        fontSelection.setOnAction(e->{
            int i = (fontSelection.getSelectionModel().getSelectedIndex());
            page.setFont(i);
            try {
                String source = sourcefolder + "font"+(i+1)+".css";
                //System.out.println(source);
                File sourcefile = new File(source);
                Path from = sourcefile.toPath();
                String des = desfolder + "font.css";
               // System.out.println(des);
                File desfile = new File(des);
                Path to = desfile.toPath();         
                Files.copy(from, to, REPLACE_EXISTING);
            } catch (IOException ex) {
                ui.getErrorHandler().processError("Error copying cont style");
            }
        });
    }
}
