/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.ComponentViews.HeaderView;
import eportfoliogenerator.ComponentViews.ImageComponentView;
import eportfoliogenerator.ComponentViews.ListComponentView;
import eportfoliogenerator.ComponentViews.ParagraphView;
import eportfoliogenerator.ComponentViews.SlideshowView;
import eportfoliogenerator.ComponentViews.VideoView;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDITOR_LABEL;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDITOR_TEXTFIELD;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDITOR_VBOX;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_TOOLBAR;
import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_GRID;

import static eportfoliogenerator.StartupConstants.ICON_ADD_HEADER;
import static eportfoliogenerator.StartupConstants.ICON_ADD_IMAGE;
import static eportfoliogenerator.StartupConstants.ICON_ADD_LIST;
import static eportfoliogenerator.StartupConstants.ICON_ADD_PARAGRAPH;
import static eportfoliogenerator.StartupConstants.ICON_ADD_SLIDE_SHOW;
import static eportfoliogenerator.StartupConstants.ICON_ADD_VIDEO;

import static eportfoliogenerator.StartupConstants.PATH_ICONS;
import static eportfoliogenerator.StartupConstants.PATH_SITE_ONE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_HEADER;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_IMAGE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_LIST;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_PARAGRAPH;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_SLIDE_SHOW;
import static eportfoliogenerator.StartupConstants.TOOLTIP_ADD_VIDEO;
import eportfoliogenerator.components.HeaderComponent;
import eportfoliogenerator.components.ImageComponent;
import eportfoliogenerator.controller.ImageSelectionController;
import eportfoliogenerator.controller.VideoSelectionController;
import eportfoliogenerator.controller.WorkspaceController;
import eportfoliogenerator.components.ImagesModel;
import eportfoliogenerator.components.ListComponent;
import eportfoliogenerator.components.ParagraphComponent;
import eportfoliogenerator.components.SlideshowComponent;
import eportfoliogenerator.components.VideoComponent;
import eportfoliogenerator.model.Page;
import eportfoliogenerator.model.PageModel;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaView;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author YanLiang Li
 */
public final class WorkspaceView extends TabPane{
    EPortfolioGeneratorView ui;
    Tab pageEditorTab;
    Tab siteViewerTab;
    private VBox pageEditVBox;
    VBox siteViewerVBox;
    ScrollPane pageEditorWorkspaceScrollPane;
    ScrollPane siteViewerWorkspaceScrollPane;
    TextField pageTitleTextField;
    TextField studentNameTextField;
    TextField footerTextField;
    HBox editButtonPane;
    MenuBar menuBar;
    Button addHeaderBt;
    Button addParagraphBt;
    Button addListBt;
    Button addImageBt;
    Button addVideoBt;
    Button addSlideshowBt;
    Button removeImageBt;
    Button removeVideoBt;
    Button removeSlideshowBt;
    Button removeListBt;
    Button removeHeaderBt;
    Button removeParagraphBt;
    Button selectImageBt;
    Button selectVideoBt;
    Button createSlideshowBt;
    Button reoveSlideshowBt;
    ImageView bannerImageView;
    ImageView imageView;
    MediaView videoView;
    WebView browser;
    WebEngine webEngine;

    ImagesModel images;
    PageModel pages;
    private ImageSelectionController imageController;
    private VideoSelectionController videoController;
    SlideShowMakerView slideUi;
    SlideShowFileManager fileManager;
    
    public WorkspaceView(EPortfolioGeneratorView initUi){
        ui = initUi;
        pages = ui.getPages();
        images = pages.getSelectedPage().getImagesModel();
        fileManager = new SlideShowFileManager();
        slideUi = new SlideShowMakerView(fileManager);
        this.initAll();
        this.getTabs().add(pageEditorTab);
        this.getTabs().add(siteViewerTab);
        //this.refreshWorkspaceView();
    }
    
    public void initAll(){
        this.initPageEditSpace();
        this.initDefualtPrompts();
        this.initHandler();
        this.initSiteViewerSpace();
    }
    
    private void initPageEditSpace(){
        pageEditorTab = new Tab();
        pageEditVBox = new VBox();
        pageEditorTab.setText("Page Editor Mode");
        
        pageEditVBox.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_VBOX);
        pageEditorWorkspaceScrollPane = new ScrollPane(pageEditVBox);
        pageEditorWorkspaceScrollPane.setFitToWidth(true);
	pageEditorWorkspaceScrollPane.setFitToHeight(true);
        pageEditorTab.setContent(pageEditorWorkspaceScrollPane);
    }
    
    private void initDefualtPrompts(){
        
        Label l1 = new Label("Page Title: ");
        l1.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_LABEL);
        Label l2 = new Label("Student Name: ");
        l2.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_LABEL);
        Label l3 = new Label("Footer: ");
        l3.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_LABEL);
        Label l4 = new Label ("Banner Image: ");
        l4.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_LABEL);
        
        pageTitleTextField = new TextField();
        pageTitleTextField.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_TEXTFIELD);
        pageTitleTextField.setText(ui.getPages().getSelectedPage().getPageTitle());
        pageTitleTextField.setMinWidth(400);
        pageTitleTextField.setMinHeight(30);
        
        studentNameTextField = new TextField();
        studentNameTextField.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_TEXTFIELD);
        studentNameTextField.setText(ui.getPages().getSelectedPage().getStudentName());
        studentNameTextField.setMinHeight(30);
        
        
        footerTextField = new TextField();
        footerTextField.getStyleClass().add(CSS_CLASS_PAGE_EDITOR_TEXTFIELD);
        footerTextField.setText(ui.getPages().getSelectedPage().getFooter());
        footerTextField.setMinWidth(400);
        footerTextField.setMinHeight(30);
        
        
        bannerImageView = new ImageView(ui.getPages().getSelectedPage().getBanner().getBannerImage());
        bannerImageView.setFitHeight(160);
        bannerImageView.setFitWidth(260);
        GridPane grid = new GridPane();
        grid.addColumn(0, l1);
        grid.addRow(0, pageTitleTextField);
        grid.addColumn(0, l2);
        grid.addRow(1, studentNameTextField);
        grid.addColumn(0, l3);
        grid.addRow(2, footerTextField);
        this.initComponentBar();
        grid.addRow(3, editButtonPane);
        StyleSelection sl = new StyleSelection(ui);
        VBox v = new VBox();
        v.getChildren().addAll(l4, bannerImageView, sl);
        v.setAlignment(Pos.CENTER);
        //grid.addColumn(2, v);
        
       // grid.addRow(2, bannerImageView);
        grid.getStyleClass().add(CSS_CLASS_PAGE_GRID);
        HBox h = new HBox();
        h.getChildren().addAll(grid, v);
        
        
        pageEditVBox.getChildren().add(h);
    }
    
    private void initComponentBar(){
        editButtonPane = new HBox();
        editButtonPane.getStyleClass().add(CSS_CLASS_PAGE_EDIT_TOOLBAR);
        addImageBt = this.initChildButton(editButtonPane, ICON_ADD_IMAGE, TOOLTIP_ADD_IMAGE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        addVideoBt = this.initChildButton(editButtonPane, ICON_ADD_VIDEO, TOOLTIP_ADD_VIDEO, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        addSlideshowBt = this.initChildButton(editButtonPane, ICON_ADD_SLIDE_SHOW, TOOLTIP_ADD_SLIDE_SHOW, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        addHeaderBt = this.initChildButton(editButtonPane, ICON_ADD_HEADER, TOOLTIP_ADD_HEADER, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
       addParagraphBt = this.initChildButton(editButtonPane, ICON_ADD_PARAGRAPH, TOOLTIP_ADD_PARAGRAPH, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
        addListBt = this.initChildButton(editButtonPane, ICON_ADD_LIST, TOOLTIP_ADD_LIST, CSS_CLASS_PAGE_EDIT_BUTTONS, false);               
    }
 
    public void reloadSitePage(){
        webEngine.reload();
    }
    private void initSiteViewerSpace(){
        siteViewerTab = new Tab();
        siteViewerVBox = new VBox();
        siteViewerTab.setText("Site View Mode");
        browser = new WebView();
        webEngine = browser.getEngine();
        String site1Path = PATH_SITE_ONE;
        File file = new File(site1Path);
        String url;
        try {
            url = file.toURI().toURL().toString();           
            webEngine.load(url);
        } catch (MalformedURLException ex) {
            ui.getErrorHandler().processError("MalformedURL");
        };
        siteViewerWorkspaceScrollPane = new ScrollPane(browser);
        siteViewerWorkspaceScrollPane.setFitToWidth(true);
	siteViewerWorkspaceScrollPane.setFitToHeight(true);
        siteViewerTab.setContent(siteViewerWorkspaceScrollPane);
        siteViewerTab.setOnSelectionChanged(e->{
            reloadSitePage();
        });
    }
    
    public VBox getPageEditorVBox(){
        return pageEditVBox;
    }
     /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     * @param toolbar
     * @param iconFileName
     * @param tooltip
     * @param cssClass
     * @param disabled
     * @return Button
     */
    public Button initChildButton(
            Pane toolbar, 
            String iconFileName, 
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        ImageView buttonImage = new ImageView (imagePath);
        buttonImage.setFitHeight(36);
        buttonImage.setFitWidth(36);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(buttonImage);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initHandler() {
        
        
        pageTitleTextField.setOnAction(e -> {
	    ui.getPages().getSelectedPage().setPageTitle(pageTitleTextField.getText());
            ui.refresh();
	});
        studentNameTextField.setOnAction(e -> {
	    ui.getPages().getSelectedPage().setStudentName(studentNameTextField.getText());
	    
	});
        footerTextField.setOnAction(e -> {
	    ui.getPages().getSelectedPage().setFooter(footerTextField.getText());
	});
        bannerImageView.setOnMousePressed(e -> {
            imageController = new ImageSelectionController(ui);
            imageController.processSelectBannerImage(ui.getPages().getSelectedPage(), this);
	});
        
        
        
        WorkspaceController wsControll = new WorkspaceController(ui, this);
        addImageBt.setOnAction(e -> {
            wsControll.processAddImageRequest();
	});
        addVideoBt.setOnAction(e -> {
	    wsControll.proceeAddVideoRequest();
	});
        addSlideshowBt.setOnAction(e -> {
	    wsControll.proceeAddSlideshowRequest();
	});
        addHeaderBt.setOnAction(e -> {
	    wsControll.proceeAddHeaderRequest();
	});
        addParagraphBt.setOnAction(e -> {
	    wsControll.proceeAddParagraphRequest();
	});
        addListBt.setOnAction(e -> {
	    wsControll.proceeAddListRequest();
	});
    }
    public void loadAll(Page page){
        int headerCount = 0;
        int imgCount = 0;
        int paraCount = 0;
        int videoCount = 0;
        int slidesCount = 0;
        int listsCount = 0;
        int headersSize = page.getHeaderModel().getHeaders().size();
        int imagesSize = page.getImagesModel().getImages().size();
        int paraSize = page.getParagraphModel().getParagraphs().size();
        int videoSize = page.getVideoModel().getVideos().size();
        int slidesSize = page.getSlideshoComponentModel().getSlideshows().size();
        int listsSize = page.getListComponentModel().getLists().size();
        this.getTabs().clear();
        this.initAll();
        
        for(String str : page.getComponentList()){
           if(str.equals("image")){
               if (imagesSize > 0 && imgCount != imagesSize){
                   loadAImage(page.getImagesModel().getImages().get(imgCount)); // image
                   imgCount++;
               }
           }
           if(str.equals("header")){
               if (headersSize > 0 && headerCount != headersSize){
                   loadAHeader(page.getHeaderModel().getHeaders().get(headerCount));
                   headerCount++;
               }
           }
           if(str.equals("paragraph")){
               if (paraSize > 0 && paraCount != paraSize){
                   loadAParagraph(page.getParagraphModel().getParagraphs().get(paraCount));
                   paraCount++;
               }
           }
           if(str.equals("video")){
               if(videoSize > 0 && videoCount != videoSize){
                   loadAVideo(page.getVideoModel().getVideos().get(videoCount));
                   videoCount++;
               }
           }
           if(str.equals("slideshow")){
               if(slidesSize > 0 && slidesCount != slidesSize){
                   loadASlideshow(page.getSlideshoComponentModel().getSlideshows().get(slidesCount));
                   slidesCount++;
               }
           }
           if(str.equals("list")){
               if(listsSize > 0 && listsCount != listsSize){
                   loadAList(page.getListComponentModel().getLists().get(listsCount));
                   listsCount++;
               }
           }
        }
        this.getTabs().add(pageEditorTab);
        this.getTabs().add(siteViewerTab);
    }
    public void loadAImage(ImageComponent img){
            ImageComponentView icv = new ImageComponentView(img, ui);
            pageEditVBox.getChildren().add(icv);
    }
    public void loadAHeader(HeaderComponent h){
            HeaderView hv = new HeaderView(h, ui);
            pageEditVBox.getChildren().add(hv);
    }
    public void loadAParagraph(ParagraphComponent p){
        ParagraphView pv = new ParagraphView(p, ui);
        pageEditVBox.getChildren().add(pv);
    }
    public void loadAVideo(VideoComponent v){
        VideoView vv = new VideoView(v, ui);
        pageEditVBox.getChildren().add(vv);
    }
    public void loadASlideshow(SlideshowComponent s){
        SlideshowView scm = new SlideshowView(s, ui);
        pageEditVBox.getChildren().add(scm);
    }
    public void loadAList(ListComponent l){
        ListComponentView lcv = new ListComponentView(l, ui);
        pageEditVBox.getChildren().add(lcv);
    }    
    public void updateBannerImage(){
        bannerImageView = new ImageView(ui.getPages().getSelectedPage().getBanner().getBannerImage());
        bannerImageView.setFitHeight(160);
        bannerImageView.setFitWidth(260);
        ui.refresh();
    }
}
