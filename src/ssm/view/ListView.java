/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import static eportfoliogenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_BUTTONS;
import static eportfoliogenerator.StartupConstants.ICON_REMOVE;
import static eportfoliogenerator.StartupConstants.TOOLTIP_REMOVE;
import eportfoliogenerator.view.EPortfolioGeneratorView;
import eportfoliogenerator.view.WorkspaceView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author YanLiang Li
 */
public class ListView {
    Stage stage;
    Scene scene;  
    VBox pane;
    Button addListBt;
    Button removeListBt;
    Button okBt;
    HBox h1;
    ObservableList<TextField> lists;
    private EPortfolioGeneratorView ui;
    private WorkspaceView workspace;
    public ListView(EPortfolioGeneratorView initUi, WorkspaceView initWorkspace){
        ui = initUi;
        workspace = initWorkspace;
        lists = FXCollections.observableArrayList();
    }
    
    public ObservableList<TextField> getList(){
        return lists;
    }
    
    public void addPage(){
        TextField textFieldToAdd = new TextField();
        lists.add(textFieldToAdd);
    }
    
    
    public void Start(){
        pane = new VBox();
       
        pane.setAlignment(Pos.TOP_CENTER);
        FlowPane fp = new FlowPane();
        addListBt = new Button("Add");
        fp.getChildren().add(addListBt);
        okBt = new Button("Done");
        fp.getChildren().add(okBt);
        pane.getChildren().add(fp);
        addListBt.setOnMousePressed(e->{
            h1 = new HBox();
            TextField tf = new TextField();
            lists.add(tf);
            Label l1 = new Label(lists.size() + "   ");
            h1.getChildren().addAll(l1, tf);
            Button removeBt = ui.initChildButton(h1, ICON_REMOVE, TOOLTIP_REMOVE, CSS_CLASS_PAGE_EDIT_BUTTONS, false);
            pane.getChildren().add(h1);
        });
        okBt.setOnMousePressed(e->{
            stage.close();
        });
        
 
        
       
        scene = new Scene(pane, 360, 360);
        stage = new Stage();
        stage.setTitle("Create a List");
        stage.setScene(scene);
        stage.showAndWait();
        
        
    }
}
